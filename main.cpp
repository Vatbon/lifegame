#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include <sys/ioctl.h>
#include <vector>
#include <cstring>
#include <time.h>
#include <string>

#define CHAR_CELL 'O'
#define CHAR_EMPTY ' '

#define DEFAULT_DELAY 50000


class Grid{
	private:
		unsigned char *map;
		unsigned char *age;
	public:
		bool is_colored = false;
		int cols, rows;
		unsigned char agecell(int, int)const;
		void live();
		unsigned char getcell(int, int)const;
		int create(int, int);
		void setcell(int, int, unsigned char);
		friend std::ostream& operator<< (std::ostream&, const Grid&);
		Grid& operator= (const Grid&);
};

Grid& Grid::operator= (const Grid& ingr){
	int i;
	if (this != &ingr){
		for (i = 0; i < rows * cols; i++){
			this->map[i] = ingr.map[i];
			this->age[i] = ingr.age[i];
		}
	}
	return *this;
}

void Grid::live(){
	int x,i,j,y,z;
	Grid next;
	next.create(rows, cols);
	for (i = 0; i < rows; i++)
		for(j = 0 ; j < cols; j++){

			next.map[i*cols + j] = map[i*cols + j];
			next.age[i*cols + j] = age[i*cols + j];

			if(map[i*cols + j] == CHAR_CELL){
				z = 0;
				for(x = -1; x < 2; x++)
					for(y = -1; y < 2; y++)
						//if( (i + x >= 0) && (i + x < rows) && (j + y >= 0) && (j + y < cols) )
							if (map[(((i + x) < 0 ? (rows - 1) : (i + x) ) % rows )*cols + (((j + y) < 0 ? (cols - 1) : (j + y)) % cols)] == CHAR_CELL)
								z++;
				z--;
				if ((z < 2)||(z > 3)){
					next.map[i*cols + j] = CHAR_EMPTY;
					next.age[i*cols + j] = 0;
				}
				else{
					next.age[i*cols + j] = age[i*cols + j] < 255 ? age[i*cols + j] + 5 : age[i*cols + j];
				}
			}
			else{
				z = 0;
				for(x = -1; x < 2; x++)
					for(y = -1; y < 2; y++)
					//	if( (i + x >= 0) && (i + x < rows) && (j + y >= 0) && (j + y < cols) )
					//		if (map[(i + x)*cols + (j + y)] == '#')
							if (map[(((i + x) < 0 ? (rows - 1) : (i + x) ) % rows )*cols + (((j + y) < 0 ? (cols - 1) : (j + y)) % cols)] == CHAR_CELL)
								z++;
				if (z == 3){
					next.map[i*cols + j] = CHAR_CELL;
					next.age[i*cols + j] = 0;
				}

			}
		}
	*this = next;
}

unsigned char Grid::getcell(int x, int y)const{
	return map[x*cols + y];
}

unsigned char Grid::agecell(int x, int y)const{
	return age[x*cols + y];
}

void Grid::setcell(int x, int y, unsigned char sym){
	map[x*cols + y] = sym;
}

int Grid::create(int irows, int icols){
	int i, j;
	rows = irows;
	cols = icols;
	map = (unsigned char*)malloc(rows*cols);
	age = (unsigned char*)malloc(rows*cols);
	for (i = 0; i < rows; i++)
		for(j = 0 ; j < cols; j++)
			map[i*cols + j] = CHAR_EMPTY;
	for (i = 0; i < rows; i++)
		for(j = 0 ; j < cols; j++)
			age[i*cols + j] = 0;
	return 0;
}

std::ostream& operator<< (std::ostream& out, const Grid& gr){
	int i, j, age;
	int r = 255, g = 0, b = 0;
	std::string color;
		//std::cout << "\033[38;2;255;0;0m"<< gr << "\033[0m";
	for (i = 0; i < gr.rows; i++){
		for(j = 0 ; j < gr.cols; j++){
			if (gr.is_colored){
			age = gr.agecell(i, j);
			r = r;
			g = age;
			b = age;
			color = ("\033[38;2;");
			color += (std::to_string(r)+";");
			color += (std::to_string(g)+";");
			color += (std::to_string(b)+"m");
		//	color = "\033[38;2;255;0;0m";
		}
			out << color << gr.getcell(i, j) << "\033[0m";
		}
		out << "\n";
	}
	return out;
}

int main(int argc, char* argv[]){
	srand((int)time(nullptr));
	Grid gr;
	bool place_manual = false;
	int childs = rand() % 1000, delay = DEFAULT_DELAY;
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	gr.create(w.ws_row - 2, w.ws_col);
	int i, j, x;
	for (i = 1; i < argc; i++){
		if(!strcmp(argv[i], "-d")){
			if (i + 1 < argc)
				delay = std::strtol(argv[i+1], nullptr, 10);
		}
		if(!strcmp(argv[i], "-s")){
			if (i + 1 < argc)
				childs = std::strtol(argv[i+1], nullptr, 10);
		}
		if(!strcmp(argv[i], "-c")){
			gr.is_colored = true;
		}
	}
		for(x = 0 ; x < childs; x++){
			i = rand() % (w.ws_row - 1);
			j = rand() % w.ws_col;
			gr.setcell(i, j, CHAR_CELL);
		}
	i = 0;
	while(1){
		system("clear");
		std::cout << gr;
		std::cout << w.ws_row-2 << "X" << w.ws_col << " Step: "<< i << std::endl;
		/*char k = getch();
		if (static_cast<int>(k) == 27)
			break;*/
		gr.live();	
		usleep(delay);
		i++;
		std::cout << "\n";
	}
	return 0;
}
